package tddt.aiserver;

import tddt.aiserver.ai.Ai;
import tddt.aiserver.ai.AiRequestHandler;
import tddt.aiserver.ai.SampleAi;
import tddt.aiserver.sockets.SimpleServer;

/**
 * Created by jpomikalek on 11/27/2015.
 */
public class AiServer
{
	public static final int DEFAULT_PORT = 9005;

	public static void main(String[] args)
	{
		Ai ai = new SampleAi();

		int port = determinePort(args);

		System.out.println("listening on port " + port);
		new SimpleServer(port, new AiRequestHandler(ai)).run();
	}

	private static int determinePort(String[] args)
	{
		if (args.length >= 1)
			return Integer.parseInt(args[0]);
		else
			return DEFAULT_PORT;
	}
}
