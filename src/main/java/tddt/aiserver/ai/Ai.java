package tddt.aiserver.ai;

import tddt.aiserver.game.Direction;
import tddt.aiserver.game.GameState;

/**
 * Created by jpomikalek on 11/9/2015.
 */
public interface Ai
{
	Direction makeMove(int aiPlayerId, GameState gameState);

	void shutdown();
}
