package tddt.aiserver.ai;

import tddt.aiserver.game.GameState;
import tddt.aiserver.serialized.SerializationUtil;
import tddt.aiserver.serialized.dto.GameStateWithPlayerIdDto;
import tddt.aiserver.sockets.SimpleRequestHandler;

/**
 * Created by jpomikalek on 11/25/2015.
 */
public class AiRequestHandler implements SimpleRequestHandler
{
	private final Ai ai;

	public AiRequestHandler(Ai ai)
	{
		this.ai = ai;
	}

	@Override
	public String processMoveRequest(String request)
	{
		try
		{
			GameStateWithPlayerIdDto gameStateWithPlayerIdDto = SerializationUtil.deserializeGameStateWithPlayerId(request);

			GameState gameState = new GameState(gameStateWithPlayerIdDto.gameState);

			return ai.makeMove(gameStateWithPlayerIdDto.aiPlayerId, gameState).toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return e.toString();
		}
	}

	@Override
	public void shutdown()
	{
		ai.shutdown();
	}
}
