package tddt.aiserver.ai;

import tddt.aiserver.game.Direction;
import tddt.aiserver.game.GameState;
import tddt.aiserver.serialized.basic.Point;

import static tddt.aiserver.game.Direction.*;

/**
 * Created by jpomikalek on 12/2/2015.
 */
public class SampleAi implements Ai
{
	@Override
	public Direction makeMove(int aiPlayerId, GameState gameState)
	{
		// Disclaimer: This is just an example.
		// Before you copy this code, be sure to check how exactly GameState#isEmptyField behaves.

		Point myPosition = gameState.getPlayerState(aiPlayerId).getHead();

		if (gameState.isEmptyField(LEFT.from(myPosition)))
			return Direction.LEFT;

		if (gameState.isEmptyField(RIGHT.from(myPosition)))
			return Direction.RIGHT;

		if (gameState.isEmptyField(UP.from(myPosition)))
			return Direction.UP;

		return Direction.DOWN;
	}

	@Override
	public void shutdown() {
	}
}
