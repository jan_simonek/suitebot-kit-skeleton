package tddt.aiserver.game;

import tddt.aiserver.serialized.basic.Point;
import tddt.aiserver.serialized.dto.GamePlanDto;

import java.util.*;

/**
 * Created by jpomikalek on 11/4/2015.
 */
public class GamePlan
{
	private final int width;
	private final int height;
	private final List<Point> startingPositions;
	private final Set<Point> walls;

	public GamePlan(GamePlanDto dto)
	{
		width = dto.width;
		height = dto.height;
		startingPositions = new ArrayList<>(dto.startingPositions);
		walls = new HashSet<>(dto.walls);
	}

	public GamePlan(int width, int height, Collection<Point> startingPositions)
	{
		this(width, height, startingPositions, Collections.emptySet());
	}

	public GamePlan(int width, int height, Collection<Point> startingPositions, Collection<Point> walls)
	{
		this.width = width;
		this.height = height;
		this.startingPositions = new ArrayList<>(startingPositions);
		this.walls = new HashSet<>(walls);

		validateStartingPositions();
	}

	public GamePlan(GamePlan gamePlan)
	{
		width = gamePlan.getWidth();
		height = gamePlan.getHeight();
		startingPositions = new ArrayList<>(gamePlan.getStartingPositions());
		walls = new HashSet<>(gamePlan.getWalls());
	}

	private void validateStartingPositions()
	{
		for (Point position : startingPositions)
		{
			if (isOutsideOfPlan(position))
				throw new IllegalStateException("starting position is outside of the plan: " + position);

			if (isWallOnPosition(position))
				throw new IllegalStateException("starting position is occupied: " + position);
		}
	}

	private boolean isOutsideOfPlan(Point position)
	{
		return position.x < 0 || position.y < 0 || position.x >= width || position.y >= height;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public boolean isWallOnPosition(Point where)
	{
		return walls.contains(where);
	}

	public void setWallOnPosition(Point position) {
		walls.add(position);
	}

	public List<Point> getStartingPositions()
	{
		return startingPositions;
	}

	Set<Point> getWalls()
	{
		return walls;
	}
}
