package tddt.aiserver.game;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import tddt.aiserver.serialized.basic.Player;
import tddt.aiserver.serialized.basic.Point;
import tddt.aiserver.serialized.dto.GameStateDto;

import java.util.*;

/**
 * Created by jpomikalek on 11/4/2015.
 */
public class GameState
{
	private final GamePlan gamePlan;
	private final List<Player> players;
	protected final Map<Integer, PlayerState> playerStateMap;
	protected final Set<Player> livePlayers;

	public GameState(GameStateDto dto)
	{
		gamePlan = new GamePlan(dto.gamePlan);
		players = dto.players;
		livePlayers = new HashSet<>(dto.livePlayers);

		playerStateMap = new HashMap<>();
		for (var entry : dto.playerStateMap.entrySet())
			playerStateMap.put(entry.getKey(), new PlayerState(entry.getValue()));
	}

	public GameState(GamePlan gamePlan, Player... players)
	{
		this(gamePlan, Arrays.asList(players));
	}

	public GameState(GamePlan gamePlan, List<Player> players)
	{
		this.gamePlan = new GamePlan(gamePlan);
		this.players = ImmutableList.copyOf(players);
		this.playerStateMap = new HashMap<>();
		this.livePlayers = new HashSet<>();

		if (players.size() != gamePlan.getStartingPositions().size())
		{
			throw new IllegalStateException(String.format("number of players (%d) is different from the number of starting positions (%d)",
					players.size(), gamePlan.getStartingPositions().size()));
		}

		for (int i = 0; i < players.size(); i++)
		{
			assertUniquePlayer(players.get(i));
			playerStateMap.put(players.get(i).id, new PlayerState(gamePlan.getStartingPositions().get(i)));
			livePlayers.add(players.get(i));
		}
	}

	public GameState(GameState gameState)
	{
		this.gamePlan = new GamePlan(gameState.getGamePlan());
		this.players = ImmutableList.copyOf(gameState.getPlayers());
		this.livePlayers = new HashSet<>(gameState.getLivePlayers());

		this.playerStateMap = new HashMap<>(players.size());
		for (Player player : players)
			playerStateMap.put(player.id, new PlayerState(gameState.getPlayerState(player)));
	}

	public PlayerState getPlayerState(Player player)
	{
		return playerStateMap.get(player.id);
	}

	public PlayerState getPlayerState(int playerId)
	{
		return playerStateMap.get(playerId);
	}

	public List<Player> getPlayers()
	{
		return players;
	}

	public Set<Player> getLivePlayers()
	{
		return ImmutableSet.copyOf(livePlayers);
	}

	public GamePlan getGamePlan()
	{
		return gamePlan;
	}

	public boolean isEmptyField(int x, int y)
	{
		return isEmptyField(new Point(x, y));
	}

	/**
	 * @return true if the position is within the game plan boundaries and the field is not occupied by any player;
	 * false otherwise
	 */
	public boolean isEmptyField(Point position)
	{
		if (position.x < 0 || position.y < 0 || position.x >= gamePlan.getWidth() || position.y >= gamePlan.getHeight())
			return false;

		if (gamePlan.isWallOnPosition(position))
			return false;

		for (var playerState : playerStateMap.values())
		{
			if (playerState.isPlayerOnPosition(position))
				return false;
		}

		return true;
	}

	public void setNotEmpty(Point position)
	{
		gamePlan.setWallOnPosition(position);
	}

	private void assertUniquePlayer(Player player)
	{
		if (livePlayers.contains(player))
			throw new IllegalStateException("duplicate player ID: " + player.id);
	}
}
