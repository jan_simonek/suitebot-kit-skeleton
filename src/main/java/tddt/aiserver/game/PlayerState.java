package tddt.aiserver.game;

import tddt.aiserver.serialized.basic.Point;
import tddt.aiserver.serialized.dto.PlayerStateDto;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by jpomikalek on 11/4/2015.
 */
public class PlayerState
{
	private final Set<Point> segments;
	private Point head;

	public PlayerState(PlayerStateDto dto)
	{
		segments = new HashSet<>(dto.segments);
		head = dto.segments.get(dto.segments.size() - 1);
	}

	public PlayerState(Point startingPosition)
	{
		segments = new HashSet<>();
		segments.add(startingPosition);
		head = startingPosition;
	}

	public PlayerState(PlayerState playerState)
	{
		segments = new HashSet<>(playerState.segments);
		head = playerState.head;
	}

	public void moveHeadTo(Point position)
	{
		segments.add(position);
		head = position;
	}

	public Point getHead()
	{
		return head;
	}

	/**
	 * Returns the coordinates of all segments.
	 */
	public Set<Point> getAllSegments()
	{
		return new HashSet<>(segments);
	}

	public boolean isPlayerOnPosition(Point position)
	{
		return segments.contains(position);
	}
}
