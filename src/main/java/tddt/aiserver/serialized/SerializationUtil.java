package tddt.aiserver.serialized;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import tddt.aiserver.serialized.dto.GameStateWithPlayerIdDto;

/**
 * Created by xpomikal on 28.11.15.
 */
public class SerializationUtil
{
	public static GameStateWithPlayerIdDto deserializeGameStateWithPlayerId(String json)
	{
		return makeGameStateGsonDeserializer().fromJson(json, GameStateWithPlayerIdDto.class);
	}

	private static Gson makeGameStateGsonDeserializer()
	{
		return new GsonBuilder()
				.enableComplexMapKeySerialization()
				.create();
	}

}
