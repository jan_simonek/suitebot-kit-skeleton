package tddt.aiserver.serialized.dto;

import com.google.gson.annotations.Expose;
import tddt.aiserver.serialized.basic.Point;

import java.util.List;
import java.util.Set;

public class GamePlanDto
{
    @Expose
    public int width;

    @Expose
    public int height;

    @Expose
    public List<Point> startingPositions;

    @Expose
    public Set<Point> walls;
}
