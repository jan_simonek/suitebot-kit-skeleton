package tddt.aiserver.serialized.dto;

import com.google.gson.annotations.Expose;
import tddt.aiserver.serialized.basic.Player;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class GameStateDto
{
    @Expose
    public GamePlanDto gamePlan;

    @Expose
    public List<Player> players;

    @Expose
    public Map<Integer, PlayerStateDto> playerStateMap;

    @Expose
    public Set<Player> livePlayers;
}
