package tddt.aiserver.serialized.dto;

import com.google.gson.annotations.Expose;

public class GameStateWithPlayerIdDto
{
    @Expose
    public int aiPlayerId;

    @Expose
    public GameStateDto gameState;
}
