package tddt.aiserver.serialized.dto;

import com.google.gson.annotations.Expose;
import tddt.aiserver.serialized.basic.Point;

import java.util.List;

public class PlayerStateDto
{
    @Expose
    public List<Point> segments;
}
