package tddt.aiserver.ai;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import tddt.aiserver.game.Direction;
import tddt.aiserver.game.GameState;
import tddt.aiserver.game.GameStateCreator;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static tddt.aiserver.game.Direction.*;

public class SampleAiTest {

    private GameState gameState;

    @Test
    public void wallsAllRoundButDown_goesDown() {

        gameState = GameStateCreator.fromString(
                "***\n" +
                "*1*\n" +
                "   \n");

        MatcherAssert.assertThat(makeMove(1), CoreMatchers.is(DOWN));
    }

    @Test
    public void wallsAndPlayersAllAround_goesToOneFreeDirection() {

        gameState = GameStateCreator.fromString(
                "* **\n" +
                "*12 \n" +
                " 34*\n" +
                "** *\n");

        assertThat(makeMove(1), equalTo(UP));
        assertThat(makeMove(2), equalTo(RIGHT));
        assertThat(makeMove(3), equalTo(LEFT));
        assertThat(makeMove(4), equalTo(DOWN));
    }

    private Direction makeMove(int playerId) {
        return new SampleAi().makeMove(playerId, gameState);
    }
}