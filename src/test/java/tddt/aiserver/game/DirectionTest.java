package tddt.aiserver.game;

import org.junit.jupiter.api.Test;
import tddt.aiserver.serialized.basic.Point;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class DirectionTest
{
	private static final Point FROM = new Point(3, 7);

	@Test
	public void leftReturnsCorrectDestination()
	{
		assertThat(Direction.LEFT.from(FROM), equalTo(new Point(2, 7)));
	}

	@Test
	public void rightReturnsCorrectDestination()
	{
		assertThat(Direction.RIGHT.from(FROM), equalTo(new Point(4, 7)));
	}

	@Test
	public void upReturnsCorrectDestination()
	{
		assertThat(Direction.UP.from(FROM), equalTo(new Point(3, 6)));
	}

	@Test
	public void downReturnsCorrectDestination()
	{
		assertThat(Direction.DOWN.from(FROM), equalTo(new Point(3, 8)));
	}
}