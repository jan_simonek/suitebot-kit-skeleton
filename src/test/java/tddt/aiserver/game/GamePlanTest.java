package tddt.aiserver.game;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tddt.aiserver.serialized.basic.Point;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class GamePlanTest {
    @Test
    public void startingPositionIsOccupiedShouldThrowException() {
        Point occupiedPosition = new Point(1, 2);

        Assertions.assertThrows(IllegalStateException.class, () ->
                new GamePlan(3, 3,
                        ImmutableList.of(occupiedPosition),
                        ImmutableSet.of(occupiedPosition)));
    }

    @Test()
    public void startingPositionOutsideOfThePlanShouldThrowException() {
        Assertions.assertThrows(IllegalStateException.class, () ->
                new GamePlan(3, 3,
                        ImmutableList.of(new Point(3, 2))));
    }

    @Test
    public void testCreateFromOtherGamePlan() {
        GamePlan plan1 = new GamePlan(3, 4,
                ImmutableList.of(new Point(1, 1), new Point(2, 2)),
                ImmutableSet.of(new Point(0, 0), new Point(0, 1)));

        GamePlan plan2 = new GamePlan(plan1);

        assertThat(plan1.getWidth(), is(plan2.getWidth()));
        assertThat(plan1.getHeight(), is(plan2.getHeight()));
        assertThat(plan1.getStartingPositions(), equalTo(plan2.getStartingPositions()));
        assertThat(plan1.getWalls(), equalTo(plan2.getWalls()));
    }
}