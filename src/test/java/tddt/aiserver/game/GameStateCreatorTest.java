package tddt.aiserver.game;

import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tddt.aiserver.serialized.basic.Point;

import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by jpomikalek on 12/2/2015.
 */
public class GameStateCreatorTest {
    @Test
    public void testCreatingFromValidString() {
        String gameStateAsString = "*1 \n" +
                "2 *\n";
        GameState gameState = GameStateCreator.fromString(gameStateAsString);

        assertThat(gameState.getGamePlan().getWidth(), is(3));
        assertThat(gameState.getGamePlan().getHeight(), is(2));

        assertFalse(gameState.isEmptyField(0, 0));
        assertFalse(gameState.isEmptyField(2, 1));

        assertTrue(gameState.isEmptyField(2, 0));
        assertTrue(gameState.isEmptyField(1, 1));

        assertThat(gameState.getPlayers().size(), is(2));

        assertThat(gameState.getPlayerState(1).getHead(), equalTo(new Point(1, 0)));
        assertThat(gameState.getPlayerState(2).getHead(), equalTo(new Point(0, 1)));
    }

    @Test
    public void invalidCharacterShouldThrowException() {
        Assertions.assertThrows(GameStateCreator.GameStateCreationException.class, () ->
                GameStateCreator.fromString("**1#"));
    }

    @Test
    public void nonRectangularPlanShouldThrowException() {
        Assertions.assertThrows(GameStateCreator.GameStateCreationException.class, () ->
                GameStateCreator.fromString("****\n****\n***\n****"));


    }
}
