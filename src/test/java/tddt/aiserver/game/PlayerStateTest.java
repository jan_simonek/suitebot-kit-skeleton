package tddt.aiserver.game;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tddt.aiserver.serialized.basic.Point;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class PlayerStateTest {


    private PlayerState playerState;

    @BeforeEach
    public void setUp() {
        playerState = new PlayerState(new Point(1, 1));
        playerState.moveHeadTo(new Point(1, 2));
        playerState.moveHeadTo(new Point(2, 2));
    }

    @Test
    public void testGetHead() {
        assertThat(playerState.getHead(), equalTo(new Point(2, 2)));
    }

    @Test
    public void testGetAllSegments() {
        Set<Point> segments = new HashSet<>();
        segments.add(new Point(1, 1));
        segments.add(new Point(1, 2));
        segments.add(new Point(2, 2));

        assertThat(playerState.getAllSegments(), equalTo(segments));
    }

    @Test
    public void testCreateFromOtherPlayerState() {
        PlayerState playerState = new PlayerState(this.playerState);
        assertThat(playerState.getHead(), equalTo(this.playerState.getHead()));
        assertThat(playerState.getAllSegments(), equalTo(this.playerState.getAllSegments()));
    }
}