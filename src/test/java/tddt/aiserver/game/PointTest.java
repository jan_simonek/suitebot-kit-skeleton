package tddt.aiserver.game;

import org.junit.jupiter.api.Test;
import tddt.aiserver.serialized.basic.Point;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class PointTest {
    @Test
    public void xHasTheValueSetInTheConstructor() {
        int x = 13;
        Point point = new Point(x, 17);

        assertThat(point.x, is(x));
    }

    @Test
    public void yHasTheValueSetInTheConstructor() {
        int y = 19;
        Point point = new Point(11, y);

        assertThat(point.y, is(y));
    }

    @Test
    public void samePointsEqual() {
        assertThat(new Point(1, 1), equalTo(new Point(1, 1)));
    }

    @Test
    public void differentPointsDoNotEqual() {
        assertThat(new Point(1, 1), not(equalTo(new Point(1, 2))));
        assertThat(new Point(1, 1), not(equalTo(new Point(2, 1))));
    }
}